
import logging

{%- if cookiecutter.command_line_name %}

import click
{%- endif %}


logger = logging.getLogger('{{cookiecutter.package_name}}')

{%- if cookiecutter.command_line_name %}

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])
{%- endif %}


def main():
    logger.info('main function')
    pass

{%- if cookiecutter.command_line_name %}


@click.command(context_settings=CONTEXT_SETTINGS)
@click.option('--input', '-i', help='Input file for {{cookiecutter.package_name}}', type=click.Path(exists=True), required=True)
@click.option('--debug', default=False, is_flag=True, help='Give more output (verbose)')
@click.version_option()
def cli(input, debug):
    """{{cookiecutter.package_name}} help"""
    sh = logging.StreamHandler()
    fmt = logging.Formatter('%(asctime)s %(name)s %(levelname)s -- %(message)s', datefmt='%H:%M:%S')
    sh.setLevel(logging.DEBUG if debug else logging.INFO)
    sh.setFormatter(fmt)
    logger.addHandler(sh)
    logger.setLevel(logging.DEBUG)
    logger.debug('Debug mode enabled')
    main()
{%- endif %}


if __name__ == "__main__":
{%- if cookiecutter.command_line_name %}
    cli()
{% else %}
    main()
{% endif %}