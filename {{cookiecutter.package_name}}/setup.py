from os import path
from setuptools import setup, find_packages


VERSION = "0.1"
DESCRIPTION = "BBGLab tool"

directory = path.dirname(path.abspath(__file__))

# Get requirements from the requirements.txt file
with open(path.join(directory, 'requirements.txt')) as f:
    required = f.read().splitlines()


# Get the long description from the README file
with open(path.join(directory, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()


setup(
    name='{{cookiecutter.package_name}}',
    version=VERSION,
    description=DESCRIPTION,
    long_description=long_description,
    long_description_content_type='text/x-rst',
    url="",
    author="Barcelona Biomedical Genomics Lab",
    author_email="bbglab@irbbarcelona.org",
    {%- if cookiecutter.license != "No license" %}
    license="{{ cookiecutter.license }}",
    {%- endif %}
    packages=find_packages(),
    install_requires=required,
    {%- if cookiecutter.command_line_name %}
    entry_points={
        'console_scripts': [
            '{{cookiecutter.command_line_name}} = {{cookiecutter.package_name}}.main:cli',
        ]
    }
    {%- endif %}
)